+++
categories = ["bisnis"]
date = "2018-03-30T10:06:28+07:00"
description = "Indra Sjafri Yusuf Mansur Management (ISYM) merupakan wujud kerjasama antara Coach Indra Sjafri dengan Ust. Yusuf Mansur."
featured = []
thumbnail = "/images/WhatsApp-Image-2018-03-13-at-10.39.57-512x384.jpeg"
slug = ""
tags = ["yusuf mansur"]
title = "PayTren Mendukung Visi Indonesia 2034 melalui ISYM"

+++
Indra Sjafri Yusuf Mansur Management (ISYM) merupakan wujud kerjasama antara Coach Indra Sjafri dengan Ust. Yusuf Mansur. Melalui I.S.Y.M, Indra Sjafri (@indrasjafri) dan Ust.Yusuf Mansur (@yusufmansurnew) akan menjalankan 3 misi besar. Pertama, menjadi konsultan dalam manajemen club sepak bola. Kedua, Football Academy yang profesional dengan cita melahirkan pemain muda yang cakap, cerdas, bermoral dan spiritual serta berkarakter dan berbudaya Indonesia. Dan Ketiga, mengemban misi melahirkan minimal 1000 pelatih sepak bola profesional.

**Awal mula pertemuan  
**Awal mula pertemuan antara Indra Sjafri dengan Ust. Yusuf Mansur yaitu di tahun 2013, pasca Timnas U-19 menjuarai Piala AFF. Ketertarikan yang sama dalam hal sepak bola menjadi jalan mereka bertemu. Namun keduanya baru kembali bertegur sapa setelah Indra Sjafri tidak lagi di PSSI. Melalui telepon, Ust. Yusuf Mansur mengajak Indra Sjafri untuk bersama membuat wadah yang mendukung PSSI dalam pengembangan pemain muda sepak bola Indonesia.

**Indonesia 2034**  
[![](https://news.treni.co.id/wp-content/uploads/2018/03/WhatsApp-Image-2018-03-13-at-10.39.56-512x640.jpeg =512x640)](https://news.treni.co.id/wp-content/uploads/2018/03/WhatsApp-Image-2018-03-13-at-10.39.56.jpeg)Kini dengan PayTren melalui I.S.Y.M akan mewadahi salah satu impian Indonesia di tahun 2034 untuk menjadi tuan rumah dan berlaga di Piala Dunia. “Mewadahi segala impian Indonesia, kami eksekusi satu-satu, kemudian sempurna. Semoga saya punya umur panjang menyaksikan Indonesia berlaga di Piala Dunia. Kita punya hashtag Indonesia2034, bukan cuma tuan rumah, tapi main di Piala Dunia,” Ujar founder I.S.Y.M, Ust. Yusuf Mansur.

sumber: [https://news.treni.co.id/paytren-mendukung-visi-indonesia-2034-melalui-i-s-y-m/](https://news.treni.co.id/paytren-mendukung-visi-indonesia-2034-melalui-i-s-y-m/ "https://news.treni.co.id/paytren-mendukung-visi-indonesia-2034-melalui-i-s-y-m/")

  